# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for the CSCI 220 Lab Task 6
* This is the initial version of this task.
* [TeamLink](https://bitbucket.org/bossDavid)

### How do I get set up? ###

* This repository only has one c plus plus program that will be used by Mohammad Julfikar Mahmud and David Tanugrah
* David Tanugrah has the adminstrator access to this repository so that he can do rw-r-w
* The first program is going to be done by Mohammad Julfikar Mahmud and David Tanugrah will do the second version as it is asked in the question.
* This c plus plus program doesn't need any database or any additional platforms or services as well.
* To run this program, user will need an IDE that can run c++ files.
* Save the file in your and open with a suitable IDE which will be used to run and edit.

### Contribution guidelines ###

* David Tanugrah created this repository and has also done other initial works.
* Mohammad Julfikar Mahmud will do rest of the part for further modification.
* CSCI 222 Lecturer Mr Faizal Alias is going to moderate this work

### Who do I talk to? ###

* davidtanugrah@gmail.com
* md.julfikar.mahmud@gmail.com