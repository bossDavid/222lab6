#include<iostream>
#include<iomanip>
using namespace std;

int main(){
    float _val,result = 0;
    short choice;
    const float pi = 3.14;

    cout<<"\t\t\t\tCSCI-222 LAB 6 Experiencing Repository Task"<<endl;
    do{
    cout<<"\n\n";
    cout<<"[>] SELECT FROM THE MENU "<<endl;
    cout<<"[1] Convert Celsius to Fahrenheit"<<endl;
    cout<<"[2] Convert Fahrenheit to Celsius"<<endl;
    cout<<"[3] Calculate Circumference of a circle"<<endl;
    cout<<"[4] Calculate Area of a circle"<<endl;
    cout<<"[5] Quit"<<endl;
    cout<<"=================================="<<endl;
    cout<<"[>] ";
    cin>>choice;

    if ((choice != 1) && (choice != 2) && (choice != 3) && (choice != 4) && (choice !=5)){
        cout<<"Wrong Input. Program Exiting"<<endl;
    }else if(choice == 5){
        return 0;
    }else{
    switch(choice){
    case 1:
        cout<<"[>]Input Celsius :";
        cin>>_val;
        result = _val*33.8;
        cout<<"In Fahrenheit -> "<<result<<endl;
        break;
    case 2:
        cout<<"[>]Input Fahrenheit :";
        cin>> _val;
        result = (_val*(-17.222));
        cout<<"In Celsius -> "<<result<<endl;
        break;

    case 3:
        cout<<"[>]Input Radius (cm):";
        cin>>_val;
        result = 2*pi*_val;
        cout<<"Circumference -> "<< result << " cm" << endl;
        break;
    case 4:
        cout <<"[>]Input Radius (cm):";
        cin>>_val;
        result = pi*_val*_val;
        cout<<"Area -> " << result << " cm2" << endl;
        break;
        }
    }
    }while (choice != 0);

    return 0;
}
